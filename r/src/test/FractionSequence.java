package test;

public class FractionSequence {
	public static void main(String [] args) {
		int input =Integer.parseInt("11");
		fc(input);
	}
	
	public static double fc(int x){
		if (x<=0)
			System.out.println("Error");
		double result=0.0;
		while (x>0){
			result=1.0/x;
			System.out.println("1/"+x+" = "+ (float)result);
			x=x-1;
		}
		return result;
	}
}
