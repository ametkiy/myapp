package test;

public class Fibonacci {
	public static void main(String [] args){
		int input =Integer.parseInt("11");
		fibonaci(input);
	}
	
	public static double fibonaci(int f){
		
		double fn=0, fn1=0, fn2=1;
		if (f<0)
			return fn;
		System.out.println("F0 = " + fn1);
		System.out.println("F1 = " + fn2);
		for (int i=2;i<f;i++){
			fn=fn1+fn2;
			fn1=fn2;
			fn2=fn;
			System.out.println("F"+i+" = " + fn);
		}
		return fn;
		
	}
}
